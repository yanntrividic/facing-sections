# facing-sections

facing-sections is a [PagedJS' hook](https://pagedjs.org/documentation/10-handlers-hooks-and-custom-javascript/) that reorders pages so that two sections face each other. It can be quite handy for biblingual projects for example, but I'm sure you'll have your own weird usecases. The two sections can be in the middle of a book with a layout that is otherwise normal, and it can also deal with an uneven amount of pages by adding blank pages where needed.

![Screenshot of the demo with the first chapter of Moby Dick. On the left page, it's in english ; on the right, in french.](https://gitlab.com/yanntrividic/facing-sections/-/raw/main/screenshot.png?ref_type=heads)

## Installation

There are a few steps you are required to take before having your two own texts looking at each other. First, make sure your `section` elements are well described, and add the hook in your HTML file somewhere after PagedJS is loaded. You'll have something like this:

```html
<section id="loomings">
    <!-- Lots of HTML -->
</section>
<section id="mirages">
    <!-- ............ -->
</section>
<script src="paged.polyfill.js"></script>
<script src="facing-sections.js"></script>
```

Then, you must specify the PagedJS [named pages](https://pagedjs.org/documentation/8-named-page/#mix-page-selectors-and-named-pages) for the two sections that you want to see face each other. You can find an example in the `style.css` file:

```css
#loomings {
    page: loomings;
    break-before: left; 
}

#mirages {
    page: mirages;
    break-before: left;
}
```

The `break-before` property is here to prevent the scenario where you have your first page on a right page — it wouldn't face the other section in that case, would it?

Finally, you must fill the page names in your `facing-sections.js` file. Like this:

```js
const leftSection = "loomings",
  rightSection = "mirages" ;
```

And you should be set! There are also a few optional `const` that you can set to your liking. You can choose there what you want to do with page numbering, folios and verbosity... Have a look!

> Also, like any PagedJS related chunk of code, it runs better on browsers that use Blink like Chrome, Chromium and ungoogled-chromium.

## Demo

This repository is a demo in itself. You can have a look at it running live [here](https://yanntrividic.fr/facing-sections).

## Acknowledgements

Thanks to Julien Taquet (@julientaq) and Fred Chasen (@fchasen) for helping me figuring how to make this piece of code work!

This hook was originally developed for [_La Morale de la Xerox_](https://gitlab.com/editionsburnaout/la-morale-de-la-xerox), published by [Éditions Burn~Août](https://editionsburnaout.fr/), a french translation of _The Moral of the Xerox_ by [Clara Lobregat Balaguer](https://hardworkinggoodlooking.com/) and [Florian Cramer](http://floriancramer.nl).

## License

Yann Trividic, licensed under GPLv3.