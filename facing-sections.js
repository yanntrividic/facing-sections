/**
 * @file Pagedjs' hook that reorganizes pages after render 
 * so that one section is on the left pages, and another one on the right pages.
 * The hook also deals with an uneven amount of pages by adding blank pages where needed.
 * Page numbers are updated so they match with the new layout.
 * 
 * /!\ Warning: this hook breaks the order of the PagedJS' CSS counter counter(page).
 * 
 * @author Yann Trividic
 * @license GPLv3
 */



/*-------- SPECIFY YOUR CUSTOM PAGEDJS NAMED PAGES FOR facing-sections --------*/

// Everything is optional except this, an error will occur if you don't specify
// your custom PagedJS page names properly here.
// The left and right section must be adjacent in your original HTML file, and the first 
const leftSection = "loomings", // Replace null by your custom page name to make it appear on the left pages.
  rightSection = "mirages" ; // Same for right pages.



/*-------- NOW TWEAK YOUR OPTIONAL SETTINGS HERE --------*/

// You are reorganizing the order of the pages.
// This script can renumber your pages for you
// (i.e. update the value of the `data-page-number` attribute).
// The old page number is then kept in the `old-data-page-number` attribute,
// and the new page number, in the `new-data-page-number` attribute.
// If for some reason you don't want to reset the data-page-number attribute, 
// you can set this value to false.
const updateDataPageNumber = true;

// If you want to add new folios, put this value to true.
// You can also change the position in the page by specifying another PagedJS class.
// New folios will have the `folio` class added to them.
// Warning: the new folios will appear on all the paged of your project,
// not only on the facing sections.
const newFolios = false,
newFoliosLocation = ".pagedjs_margin-bottom-right-corner-holder";

// Toggle verbosity in the console.
const verbose = true ;



/*-------- AND BELOW IS THE CODE FOR facing-sections --------*/

class FacingSections extends Paged.Handler {
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
    }

    pageClassLeftSection = '.pagedjs_' + leftSection + '_page' ;
    pageClassRightSection = '.pagedjs_' + rightSection + '_page' ;

    afterRendered(pages) {
      if(verbose) console.log("facing-sections is working...");

      if(leftSection === null || rightSection === null) {
        console.error("Left section and right section have to be specified. At least one of them is null.\nfacing-sections failed.");
        return;
      }

      var pagesLeft = document.querySelectorAll(this.pageClassLeftSection);
      var pagesRight = document.querySelectorAll(this.pageClassRightSection);
      
      const nbPages = Math.max(pagesLeft.length, pagesRight.length);

      if(!updateDataPageNumber && verbose) console.log("The data-page-number attributes won't be updated.");

      let pageNumberFirstFacingPage;
      try {
        pageNumberFirstFacingPage = this.getPageNumberFirstFacingPage(pagesLeft);
      } catch(e) {
        console.error("One of your custom page name couldn't be resolved into pages.\nfacing-sections failed.")
        return;
      }
        
        // add n blank pages at the end of our document
        const pagesToAdd = Math.abs(pagesLeft.length - pagesRight.length);
        // we need it if we have an uneven amount of pages between the two sections
        for(let i = 0; i < pagesToAdd; i++){
        this.chunker.addPage(true);
      } 

      for(let i = 0; i < nbPages; i++){
        let leftPage = pagesLeft.item(i);
        let leftPageNumber = pageNumberFirstFacingPage + (2*i) ;
        let rightPage = pagesRight.item(i);
        let rightPageNumber = leftPageNumber + 1 ; 

        if(leftPage != null){ // if the page exists
          this.changePageNumber(leftPage, leftPageNumber);
        } else { // otherwise we create it
          this.insertBlankPage(rightPage, true, leftPageNumber);
        }

        if(rightPage != null){
          this.changePageNumber(rightPage, rightPageNumber);
          if(leftPage != null) leftPage.insertAdjacentElement(`afterend`, rightPage);
        } else {
          this.insertBlankPage(leftPage, false, rightPageNumber);
        }
      }

      // If pages were added, then we need to update the page number of
      // all the following pages.
      if(pagesToAdd){
        const pages = document.querySelectorAll('.pagedjs_page');
        for(let i = this.getPageNumberLastFacingPage(pagesLeft, pagesRight); i < pages.length; i++) {
          this.changePageNumber(pages.item(i), i + 1); // the +1 is here because we start counting from the previous page
        }
      }
      if(newFolios) this.addNewFolios();
      if(verbose) console.log("facing-section is done.");
    }

    /**
     * Makes sure the pagedjs_left_page class is applied to this page
     * @param {pagedjs_page} page 
     */
    makePageLeft(page) {
      page.classList.remove('pagedjs_right_page');
      page.classList.add('pagedjs_left_page'); // we make sure it is a left_page
    }
    
    /**
     * Makes sure the pagedjs_right_page class is applied to this page
     * @param {pagedjs_page element} page
     */
    makePageRight(page) {
      page.classList.remove('pagedjs_left_page');
      page.classList.add('pagedjs_right_page');
    }

    /**
     * Inserts a blank page in the event that there is an uneven amount of facing pages
     * @param {pagedjs_page element} page The page element around which will be added the blank page 
     * @param {Boolean} addLeftPage If true, the page added will be a left page
     * @param {Integer} pageNumber The page number of the new blank page added
     */
    insertBlankPage(page, addLeftPage, pageNumber){
      let newPage = this.catchBlankPageAdded();
      this.changePageNumber(newPage, pageNumber);
      if(addLeftPage){
        page.insertAdjacentElement(`beforebegin`, newPage);
      } else {
        page.insertAdjacentElement(`afterend`, newPage);
      }
    }

    /**
     * In this script, we are generating the pages we will need to add at the beginning,
     * so we need to go get them at the end of the book.
     * Here is how we do it.
     * @returns a blank page
     */
    catchBlankPageAdded(){
      let page = document.querySelectorAll('.pagedjs_pages').item(0).lastChild;
      // apparently there is an object between the pagedjs_page divs that is not a div
      // we need to squeeze it.
      while(page.tagName != 'DIV'){
        page = page.previousSibling;
      }
      return page;
    }

    /**
     * Returns the page number of the first facing section's page
     * @param {List of elements} pagesLeft First facing section
     * @returns First page number
     */
    getPageNumberFirstFacingPage(pagesLeft){
      return parseInt(pagesLeft.item(0).getAttribute('data-page-number'));
    }

    /**
     * Returns the page number of the last facing section's page
     * @param {List of elements} pagesLeft facing section 1
     * @param {List of elements} pagesRight facing section 2
     * @returns Last page number
     */
     getPageNumberLastFacingPage(pagesLeft, pagesRight){
      const pages = (pagesLeft.length - pagesRight.length > 0 ? pagesLeft : pagesRight);
      const pageNumber = parseInt(pages.item(pages.length-1).getAttribute('data-page-number'));
      console.log(pageNumber);
      return pageNumber;
    }

    /**
     * Modifies the page number to make it match the new order of pages.
     * Keeps the old page number in a new attribute: old-data-page-number.
     * An even page number will give away a left page, and vice-versa.
     * 
     * @param {pagedjs_page element} page 
     * @param {Integer} pageNumber 
     */
    changePageNumber(page, pageNumber){
      // first, we make sure the page number matches a left or right page
      if(pageNumber % 2) {
        this.makePageRight(page);
      } else {
        this.makePageLeft(page);
      }

      // then we update the page number
      const oldPageNumber = page.getAttribute("data-page-number");
      if(updateDataPageNumber) page.setAttribute("data-page-number", pageNumber);

      // and add attributes consequently
      page.setAttribute("old-data-page-number", oldPageNumber);
      page.setAttribute("new-data-page-number", pageNumber);
      page.setAttribute("id", "page-" + pageNumber);
      if(verbose) console.log(`Page ${oldPageNumber} is now page ${pageNumber}`);
    }

    /**
     * Adds new folios in all the document in a div in the area
     * that has newFoliosLocation as a class name.
     */
    addNewFolios(){
      if(verbose) console.log("Now adding new folios...");
    
      var allPages = document.querySelectorAll(newFoliosLocation);
      for(let i = 1 ; i <= allPages.length ; i++){ // the last page is not taken into account
          const folio = document.createElement("div");
          folio.setAttribute("class", "folio");
          folio.innerText = i;
          allPages.item(i-1).insertAdjacentElement(`beforeend`, folio);
      }
    }
  }
  Paged.registerHandlers(FacingSections);